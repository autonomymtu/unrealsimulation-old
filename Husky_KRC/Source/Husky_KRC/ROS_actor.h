/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */
#pragma once

// Standard Includes
#include <algorithm>
#include "CoreMinimal.h"
#include "Delegates/Delegate.h"
#include "Engine/GameInstance.h"
#include "GameFramework/Actor.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ROSIntegration/Classes/RI/Topic.h"
#include "ROS_actor.generated.h"

// Creates our Twist Message Dispatcher Struct
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTwistMsgDispatched, const FVector&, linear, const FVector&, angular);

// TODO: Remove eventually, after we verify that I haven't broken everything.
UCLASS(Deprecated)
class HUSKY_KRC_API ADEPRECATED_RosActor : public AActor
{
  GENERATED_BODY()
  
public:
  // Activate our Twist Subscriber Function
  UFUNCTION(BlueprintCallable, Category="Twist Subscriber")
  virtual void twistMsgSub();

  // pointCloud2 publisher. Data port expects vector in x,y,z order concatenated.
  UFUNCTION(BlueprintCallable, Category="pointCloud2 Publisher")
  virtual void pointCloud2Msg(const float &data, FString frame_id, int32 cloud_size, int32 cloud_seq);
  
  // Transform publisher, current publishing reference point cloud transforms
  UFUNCTION(BlueprintCallable, Category="TF2 Publisher")
  virtual void tf2Msg(FTransform pt_cld_transform, FString frame_id, int32 msg_seq);

  // "GPS" position publisher. X Y Z position in FVector
  UFUNCTION(BlueprintCallable, Category="GPS position Publisher")	
  virtual void gpsOdomMsg(const FVector &position, FString frame_id, int32 msg_seq);
  
  // Wheel Odometry publisher
  UFUNCTION(BlueprintCallable, Category="Wheel odom Publisher")	
  virtual void wheelOdomMsg(const float direction, const FVector &position, const FRotator &orientation,
    const FVector &linear_twist, const FVector &angular_twist, const float &dt, FVector &angular_vel,
    FVector &linear_vel, FString frame_id, int32 msg_seq);
  
  // IMU message publisher function
  UFUNCTION(BlueprintCallable, Category="IMU msg Publisher")		
  virtual void imuMsg(const FVector &accel, const FVector &angular, const FRotator &orient, FString frame_id,
    int32 msg_seq);
    
  // Sets default values for this actor's properties
  ADEPRECATED_RosActor();
  
  // Called every frame
  virtual void Tick(float DeltaTime) override;
  
  // Twist Topic name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ROSCOM", meta=(DisplayName="Twist Subscriber Topic"))
  FString twist_sub_topic_name_;
  
  // Point Cloud Publisher Name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ROSCOM", meta=(DisplayName="Point Cloud Publish Topic"))
  FString pt_cld2_pub_name_;
  
  // TF Topic name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ROSCOM", meta=(DisplayName="TF Publish Topic"))
  FString tf2_pub_topic_name_;
  
  // GPS Odom Publisher Name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ROSCOM", meta=(DisplayName="GPS odom Publish Topic"))
  FString gps_pub_name_;
  
  // Wheel Odom Publisher Name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ROSCOM", meta=(DisplayName="Wheel odom Publish Topic"))
  FString wheel_pub_name_;
  
  // IMU Topic name
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="ROSCOM", meta=(DisplayName="IMU Publish Topic"))
  FString imu_pub_topic_name_;

  // Delegate for twist messages
  UPROPERTY(BlueprintAssignable, Category="ROSCOM", meta=(DisplayName="Twist Message Dispatcher"))
  FOnTwistMsgDispatched twist_msg_dispatcher_;

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;
  
  void OnWorldTickStart(ELevelTick TickType, float DeltaTime);

private:	

  // Topics for subscribers!
  // These need to be property'd so that they don't get garbage collected
  // https://github.com/code-iai/ROSIntegration/issues/32
  UPROPERTY()
  UTopic* twist_topic_;

  UPROPERTY()
  UTopic* imu_topic_;
  
  UPROPERTY()
  UTopic* tf2_topic_;
  
  UPROPERTY()
  UTopic* clock_topic_;
  
  UPROPERTY()
  UTopic* pt_cld2_topic_;
  
  UPROPERTY()
  UTopic* gps_topic_;
  
  UPROPERTY()
  UTopic* wheel_odom_topic_;
  
  UPROPERTY()
  FVector old_position_;
  
  UPROPERTY()
  FRotator old_orientation_;
};
