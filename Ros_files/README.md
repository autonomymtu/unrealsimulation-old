# ROS/Unreal simulation project  
##	ROS Files  
This directory contains the necessary files to and directions to launch and connect to the Unreal Simulation.  
1. On the Ubuntu machine: sudo apt-get install ros-kinetic-rosbridge-suite  
2. Make sure you have the Clearpath Husky dependencies installed http://wiki.ros.org/Robots/Husky  
3. Make sure you have the robot_localization package installed http://wiki.ros.org/robot_localization  
4. roslaunch rosbridge_server rosbridge_tcp.launch bson_only_mode:=True  
5. rosparam set use_sim_time true  
6. From the workspace in this directory, sim_ws: rosparam load sim_ws/src/sim_localization/params/robot_description.yaml robot_description  
7. cd sim_ws  
8. catkin_make  
9. source devel/setup.bash  
10. roslaunch sim_localization sim.launch  
11. run the game in Unreal editor at this point  
12. To visualize, run rviz and add in appropriate models with the fixed frame set to odom and add robot model etc. to visualize everything.  
13. Publish to the /cmd_vel topic in ROS to drive the Husky, run: rostopic list to subscribe to any topic  
    
  
## Contact
```Sam Kysar  
skysar@mtu.edu```  
  
```Parker Young  
pjyoung@mtu.edu```  