/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */
#pragma once

#include <random>
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Lidar.generated.h"

UCLASS(Blueprintable)
class HUSKY_KRC_API ALidar : public AActor
{
  GENERATED_BODY()
  
public:	
  // Sets default values for this actor's properties
  ALidar();

protected:
  // Called when the game starts or when spawned
  virtual void BeginPlay() override;

#if WITH_EDITOR
  // Called when a UPROPERTY is changed in the editor
  virtual void PostEditChangeProperty(struct FPropertyChangedEvent &Event) override;
#endif // WITH_EDITOR

public:	
  // Called every frame
  virtual void Tick(float DeltaTime) override;

  UFUNCTION(BlueprintCallable, Category="Lidar", DisplayName="Generate Full Point Cloud")
  void doFullCast(TArray<float> &points, TArray<float> &perfect_points, FTransform world_to_map_tf,
    FTransform &map_to_current_tf);

  // The Angle (-180, 180) to start scanning from. Angle is along the X axis. Must be less than the "Scan End Angle".
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Lidar", meta=(DisplayName="Scan Start Angle",
    ClampMin="-179.9"))
  float scan_start_angle_;
  
  // The Angle (-180, 180) to end scanning at. Angle is along the X axis. Must be greater than the "Scan Start Angle".
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Lidar", meta=(DisplayName="Scan End Angle", ClampMax="179.9"))
  float scan_end_angle_;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Lidar", meta=(DisplayName="Vertical FOV", ClampMin="0.0"))
  float vertical_FOV_;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Lidar", meta=(DisplayName="Distance (meters)", ClampMin="0.0"))
  float distance_;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Lidar", meta=(DisplayName="Number of Channels", ClampMin="1"))
  int32 num_channels_;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Lidar", meta=(DisplayName="Vertical Angle Offset"))
  float vert_angle_offset_;

  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Lidar", meta=(DisplayName="Horizontal Angle per Sample",
    ClampMin="0.0"))
  float h_angle_per_sample_;
  
  UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Lidar", meta=(
    DisplayName="Standard deviation of noise per Sample", ClampMin="0.0"))
  float noise_std_dev_;
  
private:
  // Calculates the height of the given
  float calculateHeight(float angle);

  // Components
  UArrowComponent* start_trace_;
  UArrowComponent* end_trace_;

  // Local variables
  FRotator starting_rotation_;
  TArray<float> height_offsets_;

  // Random stuff for noise
  // This random_device just used to seed the engines, so we may get away with just one here.
  // Also, under some C++11 implementations, multiple random_device objects may just point to the same thing anyway.
  std::random_device rd_;
  // mt19937 is slow and has a large memory footprint, but gives us the most "random" numbers
  // minstd_rand is fast and has a very small memory footprint, but is slightly less "random"
  // ranlux24 is fast and has a moderate memory footprint, and is in-between the last two engines
  std::ranlux24 gen_x_, gen_y_, gen_z_;
  // The distribution just defines how the numbers should be distributed. It's the generators that actually get the numbers.
  // Therefore, we should be able to get away with one distribution type here, as they all use the same type.
  std::normal_distribution<float> noise_dist_;
  // Need an empty list of ignored actors to give the Line Trace algorithm	
  TArray<AActor*> to_ignore_; 
};
