/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */

#include "read_write_file.h"
#include "FileHelper.h"
#include "Paths.h"

bool UReadWriteFile::loadText(FString file_name, FString& save_text)
{
  return FFileHelper::LoadFileToString(save_text, *(FPaths::ProjectDir() + file_name));
}

bool UReadWriteFile::saveText(FString save_text, FString file_name)
{
  return FFileHelper::SaveStringToFile(save_text, *(FPaths::ProjectDir() + file_name));
}
