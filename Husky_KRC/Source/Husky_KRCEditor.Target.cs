/*
 * Copyright (c) Michigan Technological University (MTU)
 * All rights reserved.
 * Part of the Automotive Research Center (ARC) project by MTU.
 * This project is distributed under the MIT License.
 * A copy should have been included with the source distribution.
 * If not, you can obtain a copy at https://opensource.org/licenses/MIT
 */

using UnrealBuildTool;
using System.Collections.Generic;

public class Husky_KRCEditorTarget : TargetRules
{
  public Husky_KRCEditorTarget(TargetInfo Target) : base(Target)
  {
    Type = TargetType.Editor;

    ExtraModuleNames.AddRange( new string[] { "Husky_KRC" } );
  }
}
